package net.ubtuni.tnc.jobs.parseTextContentItems;

import static net.ubtuni.tnc.jobs.BatchStateHandler.PARSE_TEXT_CONTENT_FLAG;

import net.ubtuni.tnc.feign.scraper.models.ArticleDAO;
import net.ubtuni.tnc.utils.BatchStatusUtils;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class ParseTextContentWriter implements ItemWriter<ArticleDAO>
{
	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		this.stepExecution = stepExecution;
	}


	@Override
	public void write(final List<? extends ArticleDAO> items) throws Exception
	{
		ArticleDAO articleDAO = items.stream().findFirst().orElseThrow(() -> new Exception("No results!"));

		ExecutionContext jobContext = stepExecution.getJobExecution().getExecutionContext();
		jobContext.putString("CONTENT", articleDAO.getContent());
		jobContext.put("ARTICLE_TITLE", articleDAO.getTitle());

		stepExecution.getJobExecution()
				.getExecutionContext()
				.put(BatchStatusUtils.STATUS, BatchStatusUtils.PublishStatus.CREATING_AUDIO);

		PARSE_TEXT_CONTENT_FLAG = true;
	}

}
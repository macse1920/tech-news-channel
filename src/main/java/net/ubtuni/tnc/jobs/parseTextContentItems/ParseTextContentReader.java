package net.ubtuni.tnc.jobs.parseTextContentItems;

import static net.ubtuni.tnc.jobs.BatchStateHandler.PARSE_TEXT_CONTENT_FLAG;

import java.util.Optional;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.job.builder.FlowBuilderException;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;

import net.ubtuni.tnc.utils.BatchStatusUtils;

public class ParseTextContentReader implements ItemReader<String>
{
	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		PARSE_TEXT_CONTENT_FLAG = false;
		this.stepExecution = stepExecution;
	}

	@Override
	public String read()
	{
		if (!PARSE_TEXT_CONTENT_FLAG)
		{
			stepExecution.getJobExecution()
					.getExecutionContext()
					.put(BatchStatusUtils.STATUS, BatchStatusUtils.PublishStatus.PARSING_CONTENT);

			return Optional.ofNullable(stepExecution.getJobExecution().getJobParameters().getString("link"))
					.orElseThrow(() -> new FlowBuilderException("Source is missing"));
		}

		return null;
	}
}
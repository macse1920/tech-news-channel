package net.ubtuni.tnc.jobs.parseTextContentItems;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import net.ubtuni.tnc.feign.scraper.clients.ArticleScraperClient;
import net.ubtuni.tnc.feign.scraper.models.ArticleDAO;

@Component
public class ParseTextContentProcessor implements ItemProcessor<String, ArticleDAO>
{

	private final ArticleScraperClient articleScraperClient;

	public ParseTextContentProcessor(final ArticleScraperClient articleScraperClient)
	{
		this.articleScraperClient = articleScraperClient;
	}


	@Override
	public ArticleDAO process(final String data) throws Exception
	{
		return articleScraperClient.getArticle(data);
	}

}
package net.ubtuni.tnc.jobs.convertTextContentToAudioFileItems;

import static net.ubtuni.tnc.jobs.BatchStateHandler.CONVERT_TEXT_TO_AUDIO_FLAG;

import net.ubtuni.tnc.feign.scraper.models.ArticleDAO;
import net.ubtuni.tnc.utils.BatchStatusUtils;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

public class ConvertTextContentToAudioFileReader implements ItemReader<ArticleDAO>
{
	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		CONVERT_TEXT_TO_AUDIO_FLAG = false;
		this.stepExecution = stepExecution;
	}

	@Override
	public ArticleDAO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException
	{
		if (!CONVERT_TEXT_TO_AUDIO_FLAG)
		{
			ExecutionContext jobContext = stepExecution.getJobExecution().getExecutionContext();
			jobContext.put(BatchStatusUtils.STATUS, BatchStatusUtils.PublishStatus.CREATING_AUDIO);

			ArticleDAO articleDAO = new ArticleDAO();

			articleDAO.setContent(stepExecution.getJobExecution().getExecutionContext()
					.getString("CONTENT"));

			return articleDAO;
		}
		return null;

	}
}
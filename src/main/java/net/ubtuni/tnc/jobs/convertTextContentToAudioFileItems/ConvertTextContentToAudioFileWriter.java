package net.ubtuni.tnc.jobs.convertTextContentToAudioFileItems;

import static net.ubtuni.tnc.jobs.BatchStateHandler.CONVERT_TEXT_TO_AUDIO_FLAG;

import java.io.File;
import java.util.List;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import net.ubtuni.tnc.utils.BatchStatusUtils;

public class ConvertTextContentToAudioFileWriter implements ItemWriter<File>
{
	private StepExecution stepExecution;

	private static final String DURATION_PROPERTY = "duration";

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		this.stepExecution = stepExecution;
	}


	@Override
	public void write(final List<? extends File> items) throws Exception
	{
		File createdAudioFile = items.stream().findFirst().orElseThrow(() -> new Exception("File not generated"));

		stepExecution.getJobExecution().getExecutionContext().put("FILE", createdAudioFile);

		stepExecution.getJobExecution()
				.getExecutionContext()
				.put(BatchStatusUtils.STATUS, BatchStatusUtils.PublishStatus.CREATING_VIDEO);

		CONVERT_TEXT_TO_AUDIO_FLAG = true;
	}

}
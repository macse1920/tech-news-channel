package net.ubtuni.tnc.jobs.convertTextContentToAudioFileItems;

import java.io.File;
import java.util.Objects;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.springframework.batch.item.ItemProcessor;

import net.ubtuni.tnc.feign.scraper.clients.Txt2SpeechClient;
import net.ubtuni.tnc.feign.scraper.models.ArticleDAO;

public class ConvertTextContentToAudioFileProcessor implements ItemProcessor<ArticleDAO, File>
{
	private Txt2SpeechClient txt2SpeechClient;

	public ConvertTextContentToAudioFileProcessor(final Txt2SpeechClient txt2SpeechClient)
	{
		this.txt2SpeechClient = txt2SpeechClient;
	}

	@Override
	public File process(final ArticleDAO item) throws Exception
	{
		String path = "/.tech-news-channel/";
		File file = new File(System.getProperty("user.home") + path + UUID.randomUUID().toString() + ".mp3");
		FileUtils.writeByteArrayToFile(file, Objects.requireNonNull(txt2SpeechClient.downloadAudioFile(item).getBody()));
		return file;
	}
}
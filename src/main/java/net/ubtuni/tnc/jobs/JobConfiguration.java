package net.ubtuni.tnc.jobs;

import java.io.File;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import net.ubtuni.tnc.feign.scraper.clients.ArticleScraperClient;
import net.ubtuni.tnc.feign.scraper.clients.Txt2SpeechClient;
import net.ubtuni.tnc.feign.scraper.clients.VideoMakerClient;
import net.ubtuni.tnc.feign.scraper.clients.YoutubeUploaderClient;
import net.ubtuni.tnc.feign.scraper.models.ArticleDAO;
import net.ubtuni.tnc.feign.scraper.models.VideoDAO;
import net.ubtuni.tnc.jobs.convertTextContentToAudioFileItems.ConvertTextContentToAudioFileProcessor;
import net.ubtuni.tnc.jobs.convertTextContentToAudioFileItems.ConvertTextContentToAudioFileReader;
import net.ubtuni.tnc.jobs.convertTextContentToAudioFileItems.ConvertTextContentToAudioFileWriter;
import net.ubtuni.tnc.jobs.parseTextContentItems.ParseTextContentProcessor;
import net.ubtuni.tnc.jobs.parseTextContentItems.ParseTextContentReader;
import net.ubtuni.tnc.jobs.parseTextContentItems.ParseTextContentWriter;
import net.ubtuni.tnc.jobs.videoMakerItems.VideoMakerProcessor;
import net.ubtuni.tnc.jobs.videoMakerItems.VideoMakerReader;
import net.ubtuni.tnc.jobs.videoMakerItems.VideoMakerWriter;
import net.ubtuni.tnc.jobs.youtubeUploaderItems.YoutubeUploaderProcessor;
import net.ubtuni.tnc.jobs.youtubeUploaderItems.YoutubeUploaderReader;
import net.ubtuni.tnc.jobs.youtubeUploaderItems.YoutubeUploaderWriter;

@Configuration
@EnableBatchProcessing
public class JobConfiguration
{

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private ArticleScraperClient articleScraperClient;

	@Autowired
	private Txt2SpeechClient txt2SpeechClient;

	@Autowired
	private VideoMakerClient videoMakerClient;

	@Autowired
	private YoutubeUploaderClient youtubeUploaderClient;

	@Bean
	public TaskExecutor threadPoolTaskExecutor()
	{

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setMaxPoolSize(12);
		executor.setCorePoolSize(8);
		executor.setQueueCapacity(15);

		return executor;
	}


	@Bean(name = "publishArticle")
	public Job publishArticle()
	{
		return jobBuilderFactory
				.get("publishArticle")
				.incrementer(new RunIdIncrementer())
				.start(parseTextContent())
				.next(convertTextContentToAudioFile())
				.next(createVideo())
				.next(uploadVideo())
				.build();
	}


	@Bean
	public Step parseTextContent()
	{
		return stepBuilderFactory.get("parseTextContent").<String, ArticleDAO>chunk(1)
				.reader(new ParseTextContentReader())
				.processor(new ParseTextContentProcessor(articleScraperClient))
				.writer(new ParseTextContentWriter())
				.build();
	}

	@Bean
	public Step convertTextContentToAudioFile()
	{
		return stepBuilderFactory.get("convertTextContentToAudioFile").<ArticleDAO, File>chunk(1)
				.reader(new ConvertTextContentToAudioFileReader())
				.processor(new ConvertTextContentToAudioFileProcessor(txt2SpeechClient))
				.writer(new ConvertTextContentToAudioFileWriter())
				.build();
	}

	@Bean
	public Step createVideo()
	{
		return stepBuilderFactory.get("createVideo").<File, File>chunk(1)
				.reader(new VideoMakerReader())
				.processor(new VideoMakerProcessor(videoMakerClient))
				.writer(new VideoMakerWriter())
				.build();

	}

	@Bean
	public Step uploadVideo()
	{
		return stepBuilderFactory.get("uploadVideo").<VideoDAO, String>chunk(1)
				.reader(new YoutubeUploaderReader())
				.processor(new YoutubeUploaderProcessor(youtubeUploaderClient))
				.writer(new YoutubeUploaderWriter())
				.build();
	}

	@Bean
	public JobLauncher asyncJobLauncher() throws Exception
	{
		SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
		jobLauncher.setJobRepository(jobRepository);
		jobLauncher.setTaskExecutor(threadPoolTaskExecutor());
		return jobLauncher;
	}
}
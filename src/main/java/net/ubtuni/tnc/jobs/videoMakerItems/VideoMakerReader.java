package net.ubtuni.tnc.jobs.videoMakerItems;

import static net.ubtuni.tnc.jobs.BatchStateHandler.VIDEO_MAKER_FLAG;

import java.io.File;
import java.util.Optional;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.job.builder.FlowBuilderException;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;

import net.ubtuni.tnc.utils.BatchStatusUtils;

public class VideoMakerReader implements ItemReader<File>
{
	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		VIDEO_MAKER_FLAG = false;
		this.stepExecution = stepExecution;
	}


	@Override
	public File read()
	{
		if (!VIDEO_MAKER_FLAG)
		{
			ExecutionContext jobContext = stepExecution.getJobExecution().getExecutionContext();
			jobContext.put(BatchStatusUtils.STATUS, BatchStatusUtils.PublishStatus.CREATING_VIDEO);

			return Optional.ofNullable((File) stepExecution.getJobExecution().getExecutionContext().get("FILE"))
					.orElseThrow(() -> new FlowBuilderException("File is missing"));
		}

		return null;
	}
}
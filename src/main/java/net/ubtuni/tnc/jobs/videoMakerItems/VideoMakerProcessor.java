package net.ubtuni.tnc.jobs.videoMakerItems;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.job.builder.FlowBuilderException;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.http.ResponseEntity;

import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.probe.FFmpegFormat;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.ubtuni.tnc.feign.scraper.clients.VideoMakerClient;

public class VideoMakerProcessor implements ItemProcessor<File, File>
{

	private final VideoMakerClient videoMakerClient;
	private StepExecution stepExecution;

	public VideoMakerProcessor(VideoMakerClient videoMakerClient)
	{
		this.videoMakerClient = videoMakerClient;
	}

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		this.stepExecution = stepExecution;
	}


	@Override
	public File process(final File audioFile) throws Exception
	{
		String articleLink = Optional.ofNullable(stepExecution.getJobExecution().getJobParameters().getString("link"))
				.orElseThrow(() -> new FlowBuilderException("Source is missing"));

		ResponseEntity entity = videoMakerClient.downloadVideoFile(articleLink, audioFile.getAbsolutePath(), getDuration(audioFile));

		return new File(Objects.requireNonNull(entity.getHeaders().get("x-final-video-path")).stream().findFirst().orElseThrow(() -> new Exception("Video not created")));
	}

	/***
	 * @param audioFile
	 * @return duration it milliseconds
	 * @throws java.io.IOException if file does not exist
	 */
	private long getDuration(final File audioFile) throws IOException, InterruptedException
	{
		FFprobe ffprobe = new FFprobe();
		FFmpegProbeResult probeResult = ffprobe.probe(audioFile.getAbsolutePath());
		FFmpegFormat format = probeResult.getFormat();
		return (long) format.duration;
	}


}
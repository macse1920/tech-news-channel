package net.ubtuni.tnc.jobs.videoMakerItems;

import static net.ubtuni.tnc.jobs.BatchStateHandler.VIDEO_MAKER_FLAG;

import java.io.File;
import java.util.List;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import net.ubtuni.tnc.utils.BatchStatusUtils;

public class VideoMakerWriter implements ItemWriter<File>
{

	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		this.stepExecution = stepExecution;
	}


	@Override
	public void write(final List<? extends File> items) throws Exception
	{
		File video = items.stream().findFirst().orElseThrow(() -> new IllegalArgumentException("Video not generated"));

		stepExecution.getJobExecution().getExecutionContext().put("FINAL_VIDEO", video.getAbsoluteFile());

		stepExecution.getJobExecution()
				.getExecutionContext()
				.put(BatchStatusUtils.STATUS, BatchStatusUtils.PublishStatus.UPLOADING_VIDEO);

		VIDEO_MAKER_FLAG = true;
	}
}
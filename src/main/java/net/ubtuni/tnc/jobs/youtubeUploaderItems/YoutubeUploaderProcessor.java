package net.ubtuni.tnc.jobs.youtubeUploaderItems;

import java.io.File;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import lombok.extern.slf4j.Slf4j;
import net.ubtuni.tnc.feign.scraper.clients.YoutubeUploaderClient;
import net.ubtuni.tnc.feign.scraper.models.VideoDAO;

@Slf4j
public class YoutubeUploaderProcessor implements ItemProcessor<VideoDAO, String>
{

	private YoutubeUploaderClient youtubeUploaderClient;
	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		this.stepExecution = stepExecution;
	}


	public YoutubeUploaderProcessor(final YoutubeUploaderClient youtubeUploaderClient)
	{
		this.youtubeUploaderClient = youtubeUploaderClient;
	}

	@Override
	public String process(final VideoDAO item) throws Exception
	{
		log.info("Trying to upload video {}", item);
		ResponseEntity<String> responseEntity = youtubeUploaderClient.upload(item.getVideoFile().getPath(),
				item.getTitle(), item.getContent());

		if (responseEntity.getStatusCode().equals(HttpStatus.OK))
		{
			log.info("Video {} has been uploaded", item);
			return responseEntity.getBody();
		}
		else
		{
			return null;
		}

	}
}
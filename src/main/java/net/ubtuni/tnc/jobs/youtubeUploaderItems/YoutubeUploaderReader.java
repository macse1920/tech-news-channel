package net.ubtuni.tnc.jobs.youtubeUploaderItems;

import static net.ubtuni.tnc.jobs.BatchStateHandler.VIDEO_MAKER_FLAG;
import static net.ubtuni.tnc.jobs.BatchStateHandler.YOUTUBE_UPLOADER_FLAG;

import java.io.File;
import java.util.Optional;


import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.job.builder.FlowBuilderException;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import net.ubtuni.tnc.feign.scraper.models.VideoDAO;
import net.ubtuni.tnc.utils.BatchStatusUtils;

public class YoutubeUploaderReader implements ItemReader<VideoDAO>
{

	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		YOUTUBE_UPLOADER_FLAG = false;
		this.stepExecution = stepExecution;
	}

	@Override
	public VideoDAO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException
	{
		if (!YOUTUBE_UPLOADER_FLAG)
		{
			stepExecution.getJobExecution()
					.getExecutionContext()
					.put(BatchStatusUtils.STATUS, BatchStatusUtils.PublishStatus.UPLOADING_VIDEO);

			String content = Optional.ofNullable((String) stepExecution.getJobExecution().getExecutionContext().get("CONTENT"))
					.orElseThrow(() -> new FlowBuilderException("File is missing"));

			String title = Optional.ofNullable((String) stepExecution.getJobExecution().getExecutionContext().get("ARTICLE_TITLE"))
					.orElseThrow(() -> new FlowBuilderException("Article is missing"));

			File video = Optional.ofNullable((File) stepExecution.getJobExecution().getExecutionContext().get("FINAL_VIDEO"))
					.orElseThrow(() -> new FlowBuilderException("File is missing"));

			VideoDAO videoDAO = new VideoDAO();
			videoDAO.setTitle(title);
			videoDAO.setContent(content);
			videoDAO.setVideoFile(video);

			return videoDAO;

		}

		return null;
	}
}
package net.ubtuni.tnc.jobs.youtubeUploaderItems;

import static net.ubtuni.tnc.jobs.BatchStateHandler.YOUTUBE_UPLOADER_FLAG;

import java.util.List;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import net.ubtuni.tnc.utils.BatchStatusUtils;

public class YoutubeUploaderWriter implements ItemWriter<String>
{

	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution)
	{
		this.stepExecution = stepExecution;
	}


	@Override
	public void write(final List<? extends String> items) throws Exception
	{
		String videoId = items.stream().findFirst().orElseThrow(() -> new Exception("Video is not uploaded"));

		stepExecution.getExecutionContext().put("video_id", videoId);

		stepExecution.getJobExecution()
				.getExecutionContext()
				.put(BatchStatusUtils.STATUS, BatchStatusUtils.PublishStatus.PUBLISHED);

		stepExecution.getJobExecution()
				.getExecutionContext()
				.put("video_id", videoId);

		YOUTUBE_UPLOADER_FLAG = true;
	}
}
package net.ubtuni.tnc.controllers;

import java.util.UUID;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.ubtuni.tnc.jobs.JobConfiguration;
import net.ubtuni.tnc.services.UploadTaskService;

@RestController
@RequestMapping(value = "upload")
public class UploadTaskController extends AbstractController
{

	@Autowired
	private UploadTaskService uploadTaskService;

	@Autowired
	private JobConfiguration jobConfiguration;

	@Autowired
	private Job processJob;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity getTasks()
	{
		try
		{
			return ResponseEntity.ok(uploadTaskService.getTasks());
		}
		catch (Exception e)
		{
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/start", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public ResponseEntity startTask(@RequestParam(value = "articleLink") String articleLink)
	{
		try
		{
			JobParameters jobParameters = new JobParametersBuilder()
					.addString("link", articleLink)
					.addString("id", UUID.randomUUID().toString())
					.toJobParameters();

			JobExecution execution = jobConfiguration.asyncJobLauncher().run(processJob, jobParameters);

			return ResponseEntity.ok(execution.getJobId());
		}
		catch (Exception e)
		{
			return ResponseEntity.badRequest().body(e.getMessage());
		}

	}
}
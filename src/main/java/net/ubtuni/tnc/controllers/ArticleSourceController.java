package net.ubtuni.tnc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import net.ubtuni.tnc.services.ArticleSourceService;

@RestController
@RequestMapping(value = "sources")
@Slf4j
public class ArticleSourceController extends AbstractController
{
	@Autowired
	private ArticleSourceService articleSourceService;

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity getSources()
	{
		try
		{
			return ResponseEntity.ok(articleSourceService.getArticleSources());
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e.getCause());
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
}
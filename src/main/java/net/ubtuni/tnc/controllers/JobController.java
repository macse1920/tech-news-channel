package net.ubtuni.tnc.controllers;

import javax.batch.operations.JobOperator;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.ubtuni.tnc.utils.BatchStatusUtils;


@RestController
@RequestMapping(value = "jobs")
public class JobController
{
	@Autowired
	JobExplorer jobExplorer;

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity getJob(@RequestParam long id)
	{
		final JobExecution jobExecution = jobExplorer.getJobExecution(id);

		if (jobExecution != null)
		{
			JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("JOB_GENERAL_STATUS", jobExecution.getStatus().toString());

			if (jobExecution.getExecutionContext().containsKey(BatchStatusUtils.STATUS))
			{
				jsonObject.addProperty("JOB_DETAILED_STATUS", jobExecution.getExecutionContext().get(BatchStatusUtils.STATUS).toString());
			}

			jsonObject.addProperty("PUBLISHED_VIDEO", jobExecution.getExecutionContext().getString("video_id", "INVALID"));


			return ResponseEntity.ok(new Gson().toJson(jsonObject));
		}

		return ResponseEntity.notFound().build();
	}
}
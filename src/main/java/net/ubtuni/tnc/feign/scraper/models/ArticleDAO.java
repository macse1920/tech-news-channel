package net.ubtuni.tnc.feign.scraper.models;

import lombok.Data;

@Data
public class ArticleDAO
{
	private String title;
	private String content;
}
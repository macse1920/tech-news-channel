package net.ubtuni.tnc.feign.scraper.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.ubtuni.tnc.feign.scraper.configuration.FeignBaseConfiguration;
import net.ubtuni.tnc.feign.scraper.models.ArticleDAO;

@FeignClient(name = "ArticleScraper", contextId = "article-scraper-client", url = "http://localhost:8768/articles",
		configuration = FeignBaseConfiguration.class)
public interface ArticleScraperClient
{
	@RequestMapping(method = RequestMethod.GET, value = "scraper/article")
	ArticleDAO getArticle(@RequestParam(name = "link") final String link);
}
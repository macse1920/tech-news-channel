package net.ubtuni.tnc.feign.scraper.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.ubtuni.tnc.feign.scraper.configuration.FeignBaseConfiguration;
import net.ubtuni.tnc.feign.scraper.models.ArticleDAO;

@FeignClient(name = "Txt2Speech", contextId = "txt-2-speech-client", url = "http://localhost:8768/txt2speech",
		configuration = FeignBaseConfiguration.class)
public interface Txt2SpeechClient
{
	@RequestMapping(value = "convert", method = RequestMethod.POST, produces = "audio/mpeg")
	ResponseEntity<byte[]> downloadAudioFile(@RequestBody final ArticleDAO articleDAO);
}

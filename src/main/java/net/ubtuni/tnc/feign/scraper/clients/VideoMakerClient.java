package net.ubtuni.tnc.feign.scraper.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "VideoMaker", contextId = "video-maker-client", url = "http://localhost:8768/videos")
public interface VideoMakerClient
{

	@RequestMapping(value = "create", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
	ResponseEntity downloadVideoFile(@RequestParam("url") String url, @RequestParam("filePath") String file,
			@RequestParam("longDuration") long audioDuration);
}
package net.ubtuni.tnc.feign.scraper.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "youtube-uploader", contextId = "youtube-uploader", url = "http://localhost:8768/upload/")
public interface YoutubeUploaderClient
{
	@RequestMapping(value = "video/upload", method = RequestMethod.POST)
	ResponseEntity<String> upload(@RequestParam("file") String videoPath, @RequestParam(value = "title") String title, @RequestBody String content);
}

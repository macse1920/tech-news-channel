package net.ubtuni.tnc.feign.scraper.models;

import java.io.File;

import lombok.Data;

@Data
public class VideoDAO
{
	private String title;
	private File videoFile;
	private String content;

}
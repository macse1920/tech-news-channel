package net.ubtuni.tnc.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UploadTask
{
	private String source;
}
package net.ubtuni.tnc.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ArticleSource
{
	private String source;
}
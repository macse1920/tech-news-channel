package net.ubtuni.tnc;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@EnableBatchProcessing
@EnableFeignClients
@EnableAsync
public class TechNewsChannelApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(TechNewsChannelApplication.class, args);
	}

}

package net.ubtuni.tnc.utils;

import org.springframework.batch.core.StepExecution;

public final class BatchStatusUtils
{
	public static final String STATUS = "batch_status";

	public enum PublishStatus
	{
		PARSING_CONTENT, CREATING_AUDIO, CREATING_VIDEO, UPLOADING_VIDEO, PUBLISHED;
	}

	public static void updateStatus(final StepExecution stepExecution, final PublishStatus status)
	{
		stepExecution.getExecutionContext().put(STATUS, status.name());
	}
}
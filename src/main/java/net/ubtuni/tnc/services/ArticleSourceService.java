package net.ubtuni.tnc.services;

import net.ubtuni.tnc.configuration.ArticleSourceConfig;
import net.ubtuni.tnc.models.ArticleSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArticleSourceService
{
	@Autowired
	private ArticleSourceConfig articleSourceConfig;

	public List<ArticleSource> getArticleSources()
	{
		return articleSourceConfig.getSources()
				.stream()
				.map(ArticleSource::new)
				.collect(Collectors.toList());
	}
}
package net.ubtuni.tnc.services;

import net.ubtuni.tnc.models.UploadTask;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UploadTaskService
{
	public List<UploadTask> getTasks()
	{
		return Arrays.asList(new UploadTask("thingy"));
	}
}